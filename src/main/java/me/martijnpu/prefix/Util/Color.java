package me.martijnpu.prefix.Util;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Tags.TagManager;
import org.jetbrains.annotations.Nullable;

public enum Color {
    DARK_GREEN("2", false),
    DARK_RED("4", false),
    LIGHT_GRAY("7", false),
    DARK_BLUE("1", false),
    GREEN("a", false),
    LIGHT_PURPLE("d", false),
    DARK_AQUA("3", false),
    DARK_PURPLE("5", false),
    GRAY("8", false),
    WHITE("f", false),
    AQUA("b", false),
    BLACK("0", false),
    RED("c", false),
    YELLOW("e", false),
    BLUE("9", false),
    GOLD("6", false),
    BOLD("l", true),
    ITALIC("o", true),
    MAGIC("k", true),
    STRIKETHROUGH("m", true),
    UNDERLINE("n", true),
    HEXADECIMAL("#", false);

    public final boolean isFormat;
    private final String colorCode;

    Color(String colorCode, boolean isFormat) {
        this.colorCode = colorCode;
        this.isFormat = isFormat;
    }

    public static @Nullable Color checkValidColor(Object sender, String colorName) {
        if (colorName.equalsIgnoreCase(HEXADECIMAL.getName())) { //Disable explicit /prefix color hexadecimal
            Messages.COLOR_INVALID.send(sender);
            return null;
        }

        if (TagManager.isHexadecimal(colorName)) {
            if (ConfigData.HEX_ENABLED.get())
                return HEXADECIMAL;
            Messages.ERROR_HEX.send(sender);
            return null;
        }

        if (ConfigData.HEX_ENABLED.get() && ConfigData.HEX_NAMES.get())
            if (HexColor.getAsHex(colorName) != null)
                return HEXADECIMAL;

        try {
            return Color.valueOf(colorName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            Messages.COLOR_INVALID.send(sender);
            return null;
        }
    }

    public String getColor() {
        return "&" + this.colorCode;
    }

    public String getColorString(String hexadecimal) {
        if (this == HEXADECIMAL)
            return hexadecimal + " " + getName();
        return getColor() + getName();
    }

    public String getName() {
        return this.toString().toLowerCase();
    }
}
