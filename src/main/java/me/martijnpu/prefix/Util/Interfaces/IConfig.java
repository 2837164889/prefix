package me.martijnpu.prefix.Util.Interfaces;

import java.util.List;
import java.util.Set;

public interface IConfig {
    boolean getBoolean(String path, boolean def);

    String getString(String path, String def);

    int getInt(String path, int def);

    List<String> getStringList(String path, List<String> def);

    Set<String> getConfigKeyList(String path, Set<String> def);

    boolean isSet(String path);

    void set(String path, Object value);

    void saveConfig();

    void reload();

    void loadLocale();
}
