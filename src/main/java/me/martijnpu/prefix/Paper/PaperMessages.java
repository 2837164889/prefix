package me.martijnpu.prefix.Paper;

import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Interfaces.IMessage;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PaperMessages implements IMessage {
    private static PaperMessages instance;

    private PaperMessages() {
    }

    public static PaperMessages getInstance() {
        if (instance == null)
            instance = new PaperMessages();
        return instance;
    }

    private static String colorMessage(String message) {
        if (message.isEmpty()) return "";
        return ChatColor.translateAlternateColorCodes('&', PaperFileManager.getInstance().getMessage(Messages.PREFIX.getPath()) + message);
    }

    @Override
    public void sendConsoleWarning(String message) {
        Main.get().getLogger().warning(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', message)));
    }

    @Override
    public void sendConsole(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    @Override
    public String getMessage(String path) {
        return PaperFileManager.getInstance().getMessage(path);
    }

    @Override
    public void sendBigMessage(Object sender, TextComponent message) {
        if (sender instanceof Player)
            ((Player) sender).spigot().sendMessage(message);
        else
            sendConsole(TextComponent.toLegacyText(message));
    }

    @Override
    public void sendMessage(Object sender, String message) {
        if (sender instanceof Player)
            ((Player) sender).sendMessage(colorMessage(message));
        else
            sendConsole(message);

    }

    @Override
    public BaseComponent[] convert(String old) {
        return TextComponent.fromLegacyText(org.bukkit.ChatColor.translateAlternateColorCodes('&', old));
    }
}
