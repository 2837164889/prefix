package me.martijnpu.prefix.Paper;

import io.papermc.paper.threadedregions.scheduler.ScheduledTask;
import me.martijnpu.prefix.Bukkit.BukkitStatics;
import me.martijnpu.prefix.EventHandler.PlaceholderAPI;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.BStatsMetrics;
import me.martijnpu.prefix.Util.Interfaces.*;
import me.martijnpu.prefix.Util.Statics;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.TimeUnit;

public class Main extends JavaPlugin implements IPrefix, Listener {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Statics.isProxy = false;
        PrefixAdapter.setAdapter(this);
        Statics.setCurrVersion(getDescription().getVersion());

        try {
            if (Integer.parseInt("%%__RESOURCE__%%") == 70359)
                Messages.PLUGIN.sendConsole("&aThanks for downloading");
        } catch (NumberFormatException ignore) {
        }

        if (LuckPermsConnector.checkInvalidVersion()) {
            this.setEnabled(false);
            return;
        }

        if (BukkitStatics.getSubVersion() >= 20.0)
            getServer().getAsyncScheduler().runAtFixedRate(this, this::checkForUpdates, 0, 1, TimeUnit.DAYS);
        else
            BukkitStatics.scheduleUpdateChecker(this);

        PaperFileManager.getInstance().printInitData();
        ConfigData.getInstance().printInitData();
        BStatsMetrics.addBukkitBStatsMeta(this);
        new PaperCommand();
        new PaperPlayerJoin();


        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null)
            PlaceholderAPI.registerPAPI(); //No message needed since PAPI is already sending a register message
        else
            Messages.PLUGIN.sendConsole("&7No PAPI found to hook into. Disabling placeholder support...");

        Messages.PLUGIN.sendConsole("&aWe're up and running");
    }

    @Override
    public void onDisable() {
        Messages.PLUGIN.sendConsole("&aDisabled successful");
    }

    private void checkForUpdates(ScheduledTask scheduledTask) {
        Statics.checkForUpdate();
    }

    @Override
    public IMessage getMessagesAdapter() {
        return PaperMessages.getInstance();
    }

    @Override
    public IConfig getConfigAdapter() {
        return PaperFileManager.getInstance();
    }

    @Override
    public IStatic getStaticsAdapter() {
        return BukkitStatics.getInstance();
    }
}
