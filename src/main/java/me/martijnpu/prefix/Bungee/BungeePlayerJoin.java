package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.Util.Statics;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class BungeePlayerJoin implements Listener {
    BungeePlayerJoin() {
        Main.get().getProxy().getPluginManager().registerListener(Main.get(), this);
    }

    @EventHandler
    public void onPlayerJoin(PostLoginEvent e) {
        Statics.onPlayerJoin(e.getPlayer());
    }
}
