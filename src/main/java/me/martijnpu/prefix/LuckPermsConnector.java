package me.martijnpu.prefix;

import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.LuckPermsResult;
import me.martijnpu.prefix.Util.Statics;
import me.martijnpu.prefix.Util.Tags.Tag;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.types.PrefixNode;
import net.luckperms.api.node.types.SuffixNode;

import java.util.UUID;

public class LuckPermsConnector {
    private static final int nodeWeight = 2147483647;
    private static LuckPermsConnector instance;
    private LuckPerms api;

    private LuckPermsConnector() {
        try {
            api = LuckPermsProvider.get();
        } catch (IllegalStateException ex) {
            api = null;
        }
    }

    private static LuckPermsConnector getInstance() {
        if (instance == null)
            instance = new LuckPermsConnector();
        return instance;
    }

    public static String getLPVersion() {
        return getInstance().api.getPluginMetadata().getVersion();
    }

    /**
     * @return Whether there is an invalid LuckPerms instance found
     */
    public static boolean checkInvalidVersion() {
        if (getInstance().api == null) {
            Messages.WARN.sendConsole("Couldn't found any valid LuckPerm v5 instance");
            Messages.WARN.sendConsole("This plugin requires LuckPerms to work!");
            Messages.WARN.sendConsole("Disabling plugin...");
            return true;
        }
        try {
            double lpVersion = Double.parseDouble(getInstance().api.getPluginMetadata().getApiVersion());
            if (lpVersion < 5) {
                Messages.WARN.sendConsole("Couldn't found any valid LuckPerm v5 instance");
                Messages.WARN.sendConsole("Please update your LuckPerms plugin!");
                Messages.WARN.sendConsole("Disabling plugin...");
                return true;
            }
            if (lpVersion < 5.4)
                Statics.useLegacyLP = true;

        } catch (NumberFormatException ex) {
            Messages.WARN.sendConsole("Couldn't found any valid LuckPerm v5 instance");
            Messages.WARN.sendConsole("Unable to determinate the API version of LuckPerms.");
            Messages.WARN.sendConsole("Please report this error to the Developer of PrefiX:");
            Messages.WARN.sendConsole("LuckPerms API version: " + getInstance().api.getPluginMetadata().getApiVersion());
            Messages.WARN.sendConsole("PrefiX version: " + Statics.currVersionString);
            Messages.WARN.sendConsole("Disabling plugin...");
            return true;
        }
        return false;
    }

    public static LuckPermsResult getPrefix(UUID uuid) {
        LuckPermsResult lpresult = new LuckPermsResult(loadUser(uuid).getCachedData().getMetaData().getPrefix());
        if (!Statics.useLegacyLP && ConfigData.KEEP_CONTEXT.get()) {
            PrefixNode node = loadUser(uuid).getCachedData().getMetaData().queryPrefix().node();
            if (node != null)
                lpresult.contextSet = node.getContexts();
        }
        return lpresult;
    }

    public static LuckPermsResult getSuffix(UUID uuid) {
        LuckPermsResult lpresult = new LuckPermsResult(loadUser(uuid).getCachedData().getMetaData().getSuffix());
        if (!Statics.useLegacyLP && ConfigData.KEEP_CONTEXT.get()) {
            SuffixNode node = loadUser(uuid).getCachedData().getMetaData().querySuffix().node();
            if (node != null)
                lpresult.contextSet = node.getContexts();
        }
        return lpresult;
    }

    static void setPrefix(UUID uuid, Tag tag) {
        PrefixNode prefixNode = PrefixNode.builder(tag.getFullTag(), nodeWeight).build();
        if (tag.contextSet != null && ConfigData.KEEP_CONTEXT.get())
            prefixNode = prefixNode.toBuilder().context(tag.contextSet).build();
        User user = loadUser(uuid);
        resetPrefix(uuid);
        user.data().add(prefixNode);
        saveUser(user);
    }

    static void setPrefix(UUID uuid, String prefix) {
        PrefixNode prefixNode = PrefixNode.builder(prefix, nodeWeight).build();
        User user = loadUser(uuid);
        resetPrefix(uuid);
        user.data().add(prefixNode);
        saveUser(user);
    }

    static void setSuffix(UUID uuid, Tag tag) {
        SuffixNode suffixNode = SuffixNode.builder(tag.getFullTag(), nodeWeight).build();
        if (tag.contextSet != null && ConfigData.KEEP_CONTEXT.get())
            suffixNode = suffixNode.toBuilder().context(tag.contextSet).build();
        User user = loadUser(uuid);
        resetSuffix(uuid);
        user.data().add(suffixNode);
        saveUser(user);
    }

    static void resetPrefix(UUID uuid) {
        try {
            User user = loadUser(uuid);
            PrefixNode prefixNode = PrefixNode.builder(getPrefix(uuid).tag, nodeWeight).build();
            user.data().clear(n -> n.getType().matches(prefixNode));
            saveUser(user);
        } catch (NullPointerException ignored) {
        }
    }

    static void resetSuffix(UUID uuid) {
        try {
            User user = loadUser(uuid);
            SuffixNode suffixNode = SuffixNode.builder(getSuffix(uuid).tag, nodeWeight).build();
            user.data().clear(n -> n.getType().matches(suffixNode));
            saveUser(user);
        } catch (NullPointerException ignored) {
        }
    }

    private static User loadUser(UUID uuid) {
        return getInstance().api.getUserManager().getUser(uuid);
    }

    private static void saveUser(User user) {
        getInstance().api.getUserManager().saveUser(user).thenRun(() -> getInstance().api.getMessagingService().ifPresent(service -> service.pushUserUpdate(user)));
    }
}
