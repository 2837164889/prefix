package me.martijnpu.prefix.EventHandler;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.martijnpu.prefix.Util.Statics;
import me.martijnpu.prefix.Util.Tags.Tag;
import me.martijnpu.prefix.Util.Tags.TagManager;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

import static me.martijnpu.prefix.Util.Statics.currVersionString;

public class PlaceholderAPI extends PlaceholderExpansion {
    /**
     * Execute check for PlaceholderAPI before executing this method!
     * This is because of the extends
     */
    public static void registerPAPI() {
        new PlaceholderAPI().register();
        new PlaceholderAPI_Suffix().register();
        Statics.isPAPIEnabled = true;
    }

    @Override
    public @NotNull String getIdentifier() {
        return "PrefiX";
    }

    @Override
    public @NotNull String getAuthor() {
        return "martijnpu";
    }

    @Override
    public @NotNull String getVersion() {
        return "v" + currVersionString;
    }

    @Override
    public boolean persist() {
        return true; // This is required or else PlaceholderAPI will unregister the Expansion on reload
    }

    @Override
    public String onRequest(OfflinePlayer player, String params) {
        Tag tag;
        /*Test all placeholders:
         /papi parse me %prefix_prefix% -- %prefix_startcolor% -- %prefix_startchar% -- %prefix_tagcolor% -- %prefix_tag% -- %prefix_endchar% -- %prefix_namecolor%
         */

        switch (params.toLowerCase()) {
            case "prefix":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.getFullTag();

            case "startcolor":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.startColor;

            case "startchar":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.startChar;

            case "tagcolor":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.getNameColor(true);

            case "tag":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.tag;

            case "endchar":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.endChar;

            case "namecolor":
                tag = TagManager.getTag(player, player, true);
                if (tag == null)
                    return "";
                return tag.endColor;

            default:
                return null; // Placeholder is unknown by the Expansion
        }
    }
}

class PlaceholderAPI_Suffix extends PlaceholderExpansion {

    @Override
    public @NotNull String getIdentifier() {
        return "SuffiX";
    }

    @Override
    public @NotNull String getAuthor() {
        return "martijnpu";
    }

    @Override
    public @NotNull String getVersion() {
        return "v" + currVersionString;
    }

    @Override
    public boolean persist() {
        return true; // This is required or else PlaceholderAPI will unregister the Expansion on reload
    }

    @Override
    public String onRequest(OfflinePlayer player, String params) {
        Tag tag;
        /*Test all placeholders:
         /papi parse me %suffix_suffix% -- %suffix_startcolor% -- %suffix_startchar% -- %suffix_tagcolor% -- %suffix_tag% -- %suffix_endchar% -- %suffix_namecolor%
         */

        switch (params.toLowerCase()) {
            case "suffix":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.getFullTag();

            case "startcolor":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.startColor;

            case "startchar":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.startChar;

            case "tagcolor":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.getNameColor(true);

            case "tag":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.tag;

            case "endchar":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.endChar;

            case "namecolor":
                tag = TagManager.getTag(player, player, false);
                if (tag == null)
                    return "";
                return tag.endColor;

            default:
                return null; // Placeholder is unknown by the Expansion
        }
    }
}
