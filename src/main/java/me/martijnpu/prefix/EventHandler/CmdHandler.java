package me.martijnpu.prefix.EventHandler;

import me.martijnpu.prefix.Core;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.FileHandler.Permission;
import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.Color;
import me.martijnpu.prefix.Util.HexColor;
import me.martijnpu.prefix.Util.Statics;
import me.martijnpu.prefix.Util.Tags.Tag;
import me.martijnpu.prefix.Util.Tags.TagManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static me.martijnpu.prefix.Core.*;
import static me.martijnpu.prefix.Util.Statics.*;

public class CmdHandler {
    private boolean isPrefix;
    private Object sender;
    private boolean isPlayer;
    private String[] args;
    private Object player;
    private Color color;

    public void onPrefixCommand(Object sender, boolean isPlayer, String[] args) {
        if (!isPlayer)
            sender = null;
        this.isPrefix = true;
        this.sender = sender;
        this.isPlayer = isPlayer;
        this.args = args;
        this.player = sender;

        if (args.length == 0) {
            Messages.CMD_UNKNOWN.send(sender, "prefix");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "help":
                onHelpCmd();
                break;

            case "color":
                onColorCmd();
                break;

            case "name":
                onNameCmd();
                break;

            case "bracket":
                onBracketCmd();
                break;

            case "list":
                ListViewer.showPrefixColorList(sender);
                break;

            case "reset":
                onResetCmd();
                break;

            case "reload":
                onReloadCmd();
                break;

            case "version":
                onVersionCmd();
                break;

            case "template":
                if (ConfigData.TEMPLATE_ENABLED.get()) {
                    if (args.length == 1) {
                        Messages.CMD_UNKNOWN.send(sender, "prefix template");
                        break;
                    }
                    args = Arrays.copyOfRange(args, 1, args.length);
                    onTemplateCommand(sender, isPlayer, args);
                    break;
                }

            case "debug":
                if (onDebugCmd())
                    break;

            default: {
                onDefaultCmd();
                break;
            }
        }
    }

    public void onSuffixCommand(Object sender, boolean isPlayer, String[] args) {
        if (!isPlayer)
            sender = null;
        this.isPrefix = false;
        this.sender = sender;
        this.isPlayer = isPlayer;
        this.args = args;
        this.player = sender;

        if (args.length == 0) {
            Messages.CMD_UNKNOWN.send(sender, "suffix");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "help":
                onHelpCmd();
                break;

            case "color":
                onColorCmd();
                break;

            case "list":
                ListViewer.showSuffixColorList(sender);
                break;

            case "reset":
                onResetCmd();
                break;

            case "remove":
                onRemoveCmd();
                break;

            case "debug":
                if (onDebugCmd())
                    break;

            default:
                onDefaultCmd();
                break;
        }
    }

    public void onTemplateCommand(Object sender, boolean isPlayer, String[] args) {
        if (!isPlayer)
            sender = null;
        this.isPrefix = true;
        this.sender = sender;
        this.isPlayer = isPlayer;
        this.args = args;
        this.player = sender;

        if (args.length == 0) {
            Messages.CMD_UNKNOWN.send(sender, "prefixtemplate");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "list":
                if (!ConfigData.TEMPLATE_ENABLED.get()) {
                    Messages.TEMPLATE_DISABLED.send(sender);
                    return;
                }
                ListViewer.showTemplateList(sender);
                break;

            case "reset":
                if (!ConfigData.TEMPLATE_ENABLED.get()) {
                    Messages.TEMPLATE_DISABLED.send(sender);
                    return;
                }
                onResetCmd();
                break;

            default:
                if (!ConfigData.TEMPLATE_ENABLED.get()) {
                    Messages.TEMPLATE_DISABLED.send(sender);
                    return;
                }

                onDefaultTemplateCmd();
                break;
        }
    }

    /**
     * Suffix & Prefix
     */
    private void onHelpCmd() {
        if (Permission.TARGET_OTHER.hasPermission(sender))
            Messages.HELP_OTHERS.sendBig(sender);
        else
            Messages.HELP.sendBig(sender);
    }

    /**
     * Suffix & Prefix
     */
    private void onColorCmd() {
        if (isPlayer && !hasAnColorPermission(sender, isPrefix ? Permission.COLOR_BASE_PREFIX : Permission.COLOR_BASE_SUFFIX)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length <= 1) {
            if (Permission.TARGET_OTHER.hasPermission(sender))
                Messages.CMD_USAGE.send(sender, (isPrefix ? "/prefix" : "/suffix") + " color <color> [name]");
            else
                Messages.CMD_USAGE.send(sender, (isPrefix ? "/prefix" : "/suffix") + " color <color>");
            return;
        }

        if (args.length >= 3) {
            player = getPlayer(sender, args[2]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.send((isPrefix ? "/prefix" : "/suffix") + " color <color> <name>");
            return;
        }

        if (args[1].equalsIgnoreCase("reset")) {
            if (isPrefix) {
                if (Core.resetPrefixColor(sender, player) && sender != player)
                    Messages.CMD_RESET_OTHER.send(player, "prefix", getDisplayName(player));
            } else {
                if (Core.resetSuffixColor(sender, player) && sender != player)
                    Messages.CMD_RESET_OTHER.send(player, "suffix", getDisplayName(player));
            }
            return;
        }

        color = Color.checkValidColor(sender, args[1]);
        if (color == null)
            return;

        if (color == Color.HEXADECIMAL) {
            String hex = HexColor.getAsHex(args[1]);
            if (hex != null)
                args[1] = hex;
        }

        if (!hasColorPermission(sender, color, isPrefix ? Permission.COLOR_BASE_PREFIX : Permission.COLOR_BASE_SUFFIX)) {
            Messages.NO_PERM_COLOR.send(sender);
            return;
        }

        if (isPrefix) {
            if (Core.changePrefixColor(sender, player, color, args[1]) && sender != player)
                Messages.COLOR_OTHER.send(sender, "prefix", getDisplayName(player));
        } else {
            if (Core.changeSuffixColor(sender, player, color, args[1]) && sender != player)
                Messages.COLOR_OTHER.send(sender, "suffix", getDisplayName(player));
        }
    }

    /**
     * Prefix Only
     */
    private void onNameCmd() {
        if (!isPrefix)
            return;

        if (isPlayer && !hasAnColorPermission(sender, Permission.COLOR_BASE_NAME)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length == 1) {
            if (Permission.TARGET_OTHER.hasPermission(sender))
                Messages.CMD_USAGE.send(sender, "/prefix name <color> [name]");
            else
                Messages.CMD_USAGE.send(sender, "/prefix name <color>");
            return;
        }

        if (args.length >= 3) {
            player = getPlayer(sender, args[2]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/prefix name <color> <name>");
            return;
        }

        if (args[1].equalsIgnoreCase("reset")) {
            if (Core.resetNameColor(sender, player) && sender != player)
                Messages.CMD_RESET_OTHER.send(sender, "name", getDisplayName(player));
            return;
        }

        color = Color.checkValidColor(sender, args[1]);
        if (color == null)
            return;

        if (color == Color.HEXADECIMAL && ConfigData.HEX_NAMES.get()) {
            String hex = HexColor.getAsHex(args[1]);
            if (hex != null)
                args[1] = hex;
        }

        if (!hasColorPermission(sender, color, Permission.COLOR_BASE_NAME)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (Core.changeNameColor(sender, player, color, args[1]) && sender != player)
            Messages.COLOR_OTHER.send(sender, "name", getDisplayName(player));
    }

    /**
     * Prefix Only
     */
    private void onBracketCmd() {
        if (!isPrefix)
            return;

        if (!ConfigData.PREFIX_BRACKET_ENABLED.get() || !ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty()) {
            Messages.ERROR_BRACKET.send(sender);
            return;
        }

        if (isPlayer && !hasAnColorPermission(sender, Permission.COLOR_BASE_BRACKET)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length == 1) {
            if (Permission.TARGET_OTHER.hasPermission(sender))
                Messages.CMD_USAGE.send(sender, "/prefix bracket <color> [name]");
            else
                Messages.CMD_USAGE.send(sender, "/prefix bracket <color>");
            return;
        }

        if (args.length >= 3) {
            player = getPlayer(sender, args[2]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/prefix bracket <color> <name>");
            return;
        }

        if (args[1].equalsIgnoreCase("reset")) {
            if (Core.resetBracketColor(sender, player) && sender != player)
                Messages.CMD_RESET_OTHER.send(sender, "bracket", getDisplayName(player));
            return;
        }

        color = Color.checkValidColor(sender, args[1]);
        if (color == null)
            return;

        if (color == Color.HEXADECIMAL && ConfigData.HEX_NAMES.get()) {
            String hex = HexColor.getAsHex(args[1]);
            if (hex != null)
                args[1] = hex;
        }

        if (!hasColorPermission(sender, color, Permission.COLOR_BASE_BRACKET)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (Core.changeBracketColor(sender, player, color, args[1]) && sender != player)
            Messages.CMD_CHANGED_OTHER.send(sender, "bracket", getDisplayName(player));
    }

    /**
     * Suffix & Prefix & Template
     */
    private void onResetCmd() {
        if (!Core.hasAnTemplatePermission(sender) && !(isPrefix ? Permission.PREFIX_CHANGE : Permission.SUFFIX_CHANGE).hasPermissionMessage(sender))
            return;

        if (args.length >= 2) {
            player = getPlayer(player, args[1]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole((isPrefix ? "/prefix" : "/suffix") + " reset <name>");
            return;
        }

        if (isPrefix) {
            Core.resetPrefix(player);
            if (player != sender)
                Messages.CMD_RESET_OTHER.send(sender, "prefix", getDisplayName(player));
        } else {
            Core.resetSuffix(player);
            if (player != sender)
                Messages.CMD_RESET_OTHER.send(sender, "suffix", getDisplayName(player));
        }
    }

    /**
     * Independent
     */
    private void onReloadCmd() {
        if (Permission.ADMIN.hasPermissionMessage(sender))
            ConfigData.getInstance().reload(sender);
    }

    /**
     * Independent
     */
    private void onVersionCmd() {
        if (!Permission.ADMIN.hasPermissionMessage(sender))
            return;

        Messages.PLUGIN.sendBig(sender, ""
                + "&fAuthor            :&a martijnpu"
                + "\n&fBungeecord     :&a " + (isProxy ? "Yes" : "No")
                + "\n&fPlaceholderAPI :&a " + (isPAPIEnabled ? "Yes" : "No")
                + "\n&fServer           :&a " + Statics.getServerVersion()
                + "\n&fLocale            :&a " + ConfigData.LOCALE.get()
                + "\n&fLuckPerms      :&a " + LuckPermsConnector.getLPVersion()
                + "\n&fYour Version    :&a " + currVersionString + (newVersion > currVersion ? " &c(outdated)" : "") + (newVersion < currVersion ? " &3(Dev Version)" : "")
                + "\n&fNewest version :&a " + (newVersion == 0 ? "N/A" : newVersionString)
                + "\n&fWebsite           :&a www.spigotmc.org/resources/prefix.70359"
                + "\n&fSupport          :&a https://discord.gg/2RHYvNy"
                + (debug ? "\n&cDEBUG MODE      &f:&4 ENABLED" : ""));
    }

    /**
     * Suffix & Prefix
     *
     * @return Debug enabled (Don't skip command)
     */
    private boolean onDebugCmd() {
        if (!Statics.debug || !Permission.ADMIN.hasPermission(sender))
            return false;

        if (args.length >= 2) {
            player = getPlayer(sender, args[1]);
            if (player == null)
                return true; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole((isPrefix ? "/prefix" : "/suffix") + " debug <name>");
            return true;
        }
        String startChar = isPrefix ? ConfigData.PREFIX_START_CHAR.get() : ConfigData.SUFFIX_START_CHAR.get();
        String endChar = isPrefix ? ConfigData.PREFIX_END_CHAR.get() : ConfigData.SUFFIX_END_CHAR.get();

        Messages.DEBUG.sendConsole("HexRegex : " + TagManager.getHexPattern(false));
        Messages.DEBUG.sendConsole("FullRegex: " + TagManager.getSplitPattern(startChar, endChar, false));

        if (isPrefix)
            Messages.DEBUG.send(sender, "Brackets: " + (ConfigData.PREFIX_BRACKET_ENABLED.get() ? "Enabled" : "Disabled"));
        Messages.DEBUG.send(sender, "Hexadecimal: " + (!ConfigData.HEX_ENABLED.get() ? "Disabled" : (!ConfigData.HEX_NAMES.get() ? "Enabled" : "Names")));
        Messages.DEBUG.send(sender, "Format: '" + ConfigData.HEX_FORMAT.get() + "'");
        Messages.DEBUG.send(sender, "");

        Tag tag = TagManager.getTag(player, player, isPrefix);
        if (tag == null)
            return true;

        Messages.DEBUG.send(sender, "NewTag: '" + tag.getFullTag().replace('&', '#') + "' --");
        return true;
    }

    /**
     * Suffix only
     */
    private void onRemoveCmd() {
        if (isPrefix)
            return;

        if (!Permission.SUFFIX_REMOVE.hasPermissionMessage(sender))
            return;

        if (args.length == 2) {
            player = getPlayer(player, args[1]);
            if (player == null)
                return; //Already handled
        }

        if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/suffix remove <name>");
            return;
        }

        Core.removeSuffix(player);
        if (player != sender)
            Messages.CMD_REMOVED_OTHER.send(sender, "suffix", getDisplayName(player));
    }

    /**
     * Suffix & Prefix
     */
    private void onDefaultCmd() {
        if (!(isPrefix ? Permission.PREFIX_CHANGE : Permission.SUFFIX_CHANGE).hasPermissionMessage(sender))
            return;

        args = AdjustArgs(args);

        if (args.length >= 2) {
            player = getPlayer(sender, args[1]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole((isPrefix ? "/prefix <prefix>" : "/suffix <suffix>") + " <name>");
            return;
        }

        if (isPrefix) {
            if (Core.changePrefix(sender, player, args[0]) && player != sender)
                Messages.CMD_CHANGED_OTHER.send(sender, "prefix", getDisplayName(player));
        } else {
            if (Core.changeSuffix(sender, player, args[0]) && player != sender)
                Messages.CMD_CHANGED_OTHER.send(sender, "suffix", getDisplayName(player));
        }
    }

    /**
     * Template only
     */
    private void onDefaultTemplateCmd() {
        if (isPlayer && !hasAnTemplatePermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        args = AdjustArgs(args);

        if (args.length >= 2) {
            player = getPlayer(sender, args[1]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/prefixtemplate <prefix> <name>");
            return;
        }

        if (Core.changeTemplate(sender, player, args[0]) && player != sender)
            Messages.CMD_CHANGED_OTHER.send(sender, "prefix", getDisplayName(player));
    }

    /**
     * Get the targeted player
     *
     * @param sender Cmd sender
     * @param name   Player to target
     * @return @Nullable Player object of target
     */
    private Object getPlayer(Object sender, String name) {
        if (sender != null && !Permission.TARGET_OTHER.hasPermission(sender)) {
            Messages.NO_PERMS_TARGET.send(sender);
            return null;
        }

        Object player = Statics.getPlayer(name);
        if (player == null) {
            Messages.ERROR_PLAYER.send(sender, name);
            return null;
        }
        return player;
    }

    /**
     * Adjust arguments if "" are used
     *
     * @param args original arguments
     * @return formatted args
     */
    private String[] AdjustArgs(String[] args) {
        List<String> newArgs = new ArrayList<>();
        StringBuilder combinedString = new StringBuilder();
        boolean combine = false;

        for (String arg : args) {
            if (arg.equalsIgnoreCase("\"")) { //behaviour when only an " is found
                combine = !combine;
                combinedString.append(' ');

                if (!combine) {
                    newArgs.add(combinedString.toString());
                    combinedString = new StringBuilder();
                }
                continue;
            }

            if (arg.startsWith("\"")) {
                combine = true;
                combinedString.append(arg.substring(1)); //remove " at beginning
            } else if (combine)
                combinedString.append(' ').append(arg);
            else
                newArgs.add(arg); //default behaviour

            if (combine && arg.endsWith("\"")) {
                combine = false;
                combinedString.deleteCharAt(combinedString.length() - 1); //remove " at end
                newArgs.add(combinedString.toString());
                combinedString = new StringBuilder();
            }
        }
        if (combinedString.length() > 0)
            newArgs.add(combinedString.toString());

        return newArgs.toArray(new String[0]);
    }
}
