package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Interfaces.ICommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NewBukkitCommand extends ICommand {
    public void registerBukkitCommands(Plugin plugin) {
        List<Command> cmd = Arrays.asList(
                new org.bukkit.command.defaults.BukkitCommand("prefix", "PrefiX command", "/prefix", Arrays.asList("prefix", "tag")) {
                    @Override
                    public boolean execute(@NotNull CommandSender sender, @NotNull String commandLabel, @NotNull String[] args) {
                        return onPrefixCommand(sender, (sender instanceof Player), args);
                    }

                    @Override
                    public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) {
                        return onPrefixTabComplete(sender, args);
                    }
                },
                new org.bukkit.command.defaults.BukkitCommand("suffix", "SuffiX command", "/suffix", Collections.singletonList("suffix")) {
                    @Override
                    public boolean execute(@NotNull CommandSender sender, @NotNull String commandLabel, @NotNull String[] args) {
                        return onSuffixCommand(sender, (sender instanceof Player), args);
                    }

                    @Override
                    public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) {
                        return onSuffixTabComplete(sender, args);
                    }
                },
                new org.bukkit.command.defaults.BukkitCommand("prefixtemplate", "PrefiX template command", "/prefixtemplate", Arrays.asList("prefixtemplate", "prefixtemp")) {
                    @Override
                    public boolean execute(@NotNull CommandSender sender, @NotNull String commandLabel, @NotNull String[] args) {
                        return onTemplateCommand(sender, (sender instanceof Player), args);
                    }

                    @Override
                    public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) {
                        return onTemplateTabComplete(sender, args);
                    }
                });

        try {
            Bukkit.getCommandMap().registerAll(plugin.getDescription().getName(), cmd);
        } catch (NoSuchMethodError ex) { //Older MC versions doesn't support the clean way
            try {
                Messages.PLUGIN.sendConsole("&7Switching back to legacy command loading...");

                final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
                bukkitCommandMap.setAccessible(true);
                CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
                commandMap.registerAll(plugin.getDescription().getName(), cmd);
            } catch (NoSuchFieldException | IllegalAccessException ex2) {
                Messages.WARN.sendConsole("Please report this error to the developer:\n" + ex.getLocalizedMessage());
            }
        }
    }
}
