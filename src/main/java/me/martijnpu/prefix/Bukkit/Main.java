package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.EventHandler.PlaceholderAPI;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.BStatsMetrics;
import me.martijnpu.prefix.Util.Interfaces.*;
import me.martijnpu.prefix.Util.Statics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements IPrefix {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Statics.isProxy = false;
        PrefixAdapter.setAdapter(this);
        Statics.setCurrVersion(getDescription().getVersion());

        try {
            if (Integer.parseInt("%%__RESOURCE__%%") == 70359)
                Messages.PLUGIN.sendConsole("&aThanks for downloading");
        } catch (NumberFormatException ignore) {
        }

        if (LuckPermsConnector.checkInvalidVersion()) {
            this.setEnabled(false);
            return;
        }

        BukkitStatics.scheduleUpdateChecker(this);

        BukkitFileManager.getInstance().printInitData();
        ConfigData.getInstance().printInitData();
        BStatsMetrics.addBukkitBStatsMeta(this);
        new BukkitPlayerJoin();

        if (BukkitStatics.getSubVersion() < 20.0)
            new BukkitCommand(); //Still use plugin.yml
        else
            new NewBukkitCommand().registerBukkitCommands(this);

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null)
            PlaceholderAPI.registerPAPI(); //No message needed since PAPI is already sending a register message
        else
            Messages.PLUGIN.sendConsole("&7No PAPI found to hook into. Disabling placeholder support...");

        Messages.PLUGIN.sendConsole("&aWe're up and running");
    }

    @Override
    public void onDisable() {
        Messages.PLUGIN.sendConsole("&aDisabled successful");
    }

    @Override
    public IMessage getMessagesAdapter() {
        return BukkitMessages.getInstance();
    }

    @Override
    public IConfig getConfigAdapter() {
        return BukkitFileManager.getInstance();
    }

    @Override
    public IStatic getStaticsAdapter() {
        return BukkitStatics.getInstance();
    }
}
